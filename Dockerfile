FROM python:3.6-stretch
RUN apt-get update
RUN apt-get install -y gcc default-libmysqlclient-dev libmariadbd-dev python-mysqldb libpq-dev
RUN pip install --upgrade pip
ADD requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt
