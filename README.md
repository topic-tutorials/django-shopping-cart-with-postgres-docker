# Simple Django Shopping Cart using Django, PostgreSQL and Docker

## Installation on Linux-like OS / Mac OS / Windows OS
* Docker
* Docker-Compose 

## Services

* Simple Django Shopping Cart Service
* PostgreSQLdjango-shopping-cart-with-postgres-docker
* PGAdmin4(Web Client)

## Use Docker to run simple Django Shopping Cart 

> mkdir -p C:\django-workspace\

> cd C:\django-workspace\

> git clone https://gitlab.com/topic-tutorials/django-shopping-cart-with-postgres-docker.git

> cd django-shopping-cart-with-postgres-docker

> docker-compose build

> docker-compose up

## Create Admin user of Simple Django Shopping Cart

> docker-compose exec django python /code/manage.py createsuperuser
~~~
Username (leave blank to use 'root'): willis
Email address: misweyu2007@gmail.com
Password: 
Password (again): 
Superuser created successfully.
~~~

## Try to use Simple Django Shopping Cart and its Admin dashboard

> http://localhost:8001

> http://localhost:8001/admin

> http://localhost:8888/
